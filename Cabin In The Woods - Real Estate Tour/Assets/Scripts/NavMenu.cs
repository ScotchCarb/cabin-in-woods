using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavMenu : MonoBehaviour
{
    [SerializeField] private GameObject navMenu;
    [SerializeField] private GameObject welcome;
    [SerializeField] private MouseLook mouseLook;
    [SerializeField] private GameObject mapCam;

    [SerializeField] private List<Image> mapIcons;

    private NavigationWaypoint wpoint;

    private void Awake()
    {
        navMenu.SetActive(false);
        welcome.SetActive(true);
        mapCam.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            ToggleMenu();
        }
    }

    public void TeleportPlayer(GameObject waypoint)
    {      
        if(Interaction.Instance.CurrentWaypoint != waypoint)
        {
            if (waypoint.TryGetComponent(out wpoint) == true)
            {               
                ToggleMenu();                
                wpoint.Activate();               
            }            
        }
    }

    public void ToggleMenu()
    {
        if (navMenu.activeSelf != true)
        {
            navMenu.SetActive(true);
            mouseLook.ToggleMouseLook(false);
            mapCam.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            foreach (Image icon in mapIcons)
            {
                icon.color = Color.white;
            }
        }
        else
        {
            navMenu.SetActive(false);
            mouseLook.ToggleMouseLook(true);
            mapCam.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

    }

    public void ToggleWelcome()
    {
        
            welcome.SetActive(false);
            mouseLook.ToggleMouseLook(true);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        

    }
}
