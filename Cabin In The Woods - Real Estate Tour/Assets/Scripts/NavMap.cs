using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NavMap : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image mapIcon;   

public void OnPointerEnter(PointerEventData eventData)
    {
        mapIcon.color = Color.green;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mapIcon.color = Color.white;
    }
}
