using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour
{
    [SerializeField] List<GameObject> toolTips;

    [SerializeField] Text currentScore;

    private int score;
    private int scoreMax;

    private void Awake()
    {
        //assign the number of tooltips to the maximum possible score
        if(toolTips != null)
        {
            scoreMax = toolTips.Count;
        }
        else
        {
            //if there are no tooltips in the list display an error message
            //Debug.LogError("You need to put tooltips into toolTips!");
        }
        //set the initial score to zero
        score = 0;

        if (currentScore != null)
        {
            //set the text for the currentscore display in the UI to 0 / maximum number of tooltips
            currentScore.text = score + " / " + scoreMax + " Tooltips Found!";
        }
        else
        {
            //display error message if there isn't an assigned Text object
            //Debug.LogError("You need to place a Text object in currentScore!");
        }
    }

    //increase the current score by 1 and update the UI display
    public void IncreaseScore()
    {
        score++;
        currentScore.text = score + " / " + scoreMax + " Tooltips Found!";
    }

}
